﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logia
{
    public abstract class Ortesis
    {
        public int CodigoAlfaNumeric { get; set; }
        public Color ColorOrtesis { get; set; }
        public enum Color
        {
            Blanco,
            Azul,
            Verde,
            Negro
        }
        public Material MaterialConfeccion { get; set; }
        public enum Material
        {
            Termoplastico,
            Venda,
            Mixto
        }
        public double PrecioUnitario { get; set; }
        public int StockActualizado { get; set; }
        public string Caracteristicas { get; set; }
        public double Acumulador { get; set; }
        public virtual double CalcularPorcentajeRecargo()
        {
            if(MaterialConfeccion== Material.Termoplastico)
            {
                return Acumulador = PrecioUnitario * 0.03;
            }
            return Acumulador = 0;
        }
        public int DescontarStockActualizado()
        {
           return StockActualizado -= 1;
        }

        public abstract string ObtenerDscripcion(string nombreProvincia, DateTime fecha1, DateTime fecha2);
    }
}
