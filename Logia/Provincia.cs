﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logia
{
    public class Provincia
    {
        public int CodigoProvincia { get; set;}
        public string NombreProvincia { get; set; }
    }
}
