﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logia
{
    class Principal
    {
        List<Ortesis> Ortesises = new List<Ortesis>();
        List<Venta> Ventas = new List<Venta>();
        List<Cliente> Clientes = new List<Cliente>();
        List<Localidad> Localidades = new List<Localidad>();

        public string RegistarCarga(int codigoAlfanumerico, int color, int materialConfeccion, double precioUnitario, int stock, string caracteristicas)
        {
            Ortesis nuevaOrtesis = Ortesises.Find(x => x.CodigoAlfaNumeric == codigoAlfanumerico);
            if (nuevaOrtesis != null)
            {
                nuevaOrtesis.CodigoAlfaNumeric = codigoAlfanumerico;
                nuevaOrtesis.ColorOrtesis = (Ortesis.Color)color;
                nuevaOrtesis.MaterialConfeccion = (Ortesis.Material)materialConfeccion;
                nuevaOrtesis.PrecioUnitario = precioUnitario;
                nuevaOrtesis.StockActualizado = stock;
                nuevaOrtesis.Caracteristicas = caracteristicas;
                Ortesises.Add(nuevaOrtesis);
                return "La carga del nuevo producto se realizo con exito";
            }
            return "No se pudo registrar el producto porque su codigo ya se encuentra en el sistema";
        }


        public void RegistrarVenta(int codigoOrtesis, int dniCliente)
        {
            Venta nuevaVenta = new Venta();
            Ortesis ortesis = Ortesises.Find(x => x.CodigoAlfaNumeric == codigoOrtesis);

                nuevaVenta.CodigoOrteosisVendida = codigoOrtesis;
                nuevaVenta.DNICliente = dniCliente;
                nuevaVenta.FechaVenta = DateTime.Now;
                nuevaVenta.PorcentajeRecargoAplicado = ortesis.CalcularPorcentajeRecargo();
                nuevaVenta.PrecioTotalCalculado = (ortesis.PrecioUnitario + ortesis.CalcularPorcentajeRecargo());
                ortesis.StockActualizado = ortesis.DescontarStockActualizado();
                Ventas.Add(nuevaVenta);   
        }

        public List<Cliente> ClientesXProvincia(string nombreProvincia)
        {
            foreach (Localidad item in Localidades)
            {
                if (item.ProvinciaAsociada.NombreProvincia == nombreProvincia)
                {
                    List<Cliente> client1 = Clientes.FindAll(x => x.CodigoPostal == item.CodigoPostal);
                    return client1;
                }
            }
            return null;
        }
        public List<string> GenerarListaVentaXProvincia(string nombreProvincia, DateTime fecha1, DateTime fecha2)
        {
            List<Cliente> listAuxiliar= ClientesXProvincia(nombreProvincia);
            foreach (Venta item in Ventas)
            {
                
            }
        }

    }
}
