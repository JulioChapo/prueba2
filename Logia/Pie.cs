﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logia
{
    public class Pie: Ortesis
    {
        public int Talle { get; set; }
        public bool AutoAjustable { get; set; }
        public bool MovimientoActivo { get; set; }
    }
}
