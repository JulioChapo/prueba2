﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logia
{
    public class Rodilla: Ortesis
    {
        public int LargoTotal { get; set; }
        public bool SoporteRotula { get; set; }
        public int DiametroInferior { get; set; }
        public int DiametroSuperior { get; set; }

        public override double CalcularPorcentajeRecargo()
        {
            if (SoporteRotula == true)
            {                  
               return Acumulador += PrecioUnitario * 0.04;
            }
            return Acumulador;
        }

        public override string ObtenerDscripcion(string nombreProvincia, DateTime fecha1, DateTime fecha2)
        {
            throw new NotImplementedException();
        }
    }
}
