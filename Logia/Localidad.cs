﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logia
{
    public class Localidad
    {
        public int CodigoPostal { get; set; }
        public string NombreLocalidad { get; set; }
        public Provincia ProvinciaAsociada { get; set; }
    }
}
